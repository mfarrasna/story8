from django.test import TestCase

# Create your tests here.
from django.test import TestCase,Client

# Create your tests here.
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone

from .views import book 
from project8.settings import BASE_DIR

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time
import unittest
import json
import os

class BookAppTest(TestCase):
    def test_apakah_landing_page_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_jika_landing_page_tidak_ada(self):
        response = Client().get('/otherurl')
        self.assertEqual(response.status_code, 404)

    def test_fungsi_landing_page(self):
        found = resolve('/')
        self.assertEqual(found.func, book)
    
    def test_ada_template_landingpage(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'book.html')
    
    def test_ada_search_box(self):
        response = Client().get('/')
        content = response.content.decode()
        self.assertIn("Search</button>", content)

class FunctionalTest(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,'chromedriver'), chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
    
    def test_search(self):
        driver = self.browser
        driver.get('http://localhost:8000/')
        box_input = driver.find_element_by_id("input")
        time.sleep(4)
        box_input.send_keys("biologi")
        time.sleep(4)
        button_input = driver.find_element_by_id("put")
        button_input.click()
        time.sleep(2)
        self.assertIn("Biologi", driver.page_source)
        time.sleep(3)
    


