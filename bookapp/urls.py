from django.urls import path
from . import views

app_name = 'bookapp'

urlpatterns = [
    path('', views.book, name='bookapp'),
    path('listbuku/<str:value>',views.get_list_buku_JSON, name='listbuku')
]