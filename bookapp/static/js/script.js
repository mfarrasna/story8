$(document).ready(function(){
    $('#put').click(function(){
         
        //var keycode = (event.keyCode ? event.keyCode : event.which); 
        var data_buku_default=''
        var search = $('#input').val()
        $.ajax({
            async: true,
            type: 'GET',
            url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
            dataType: 'json',
            success: function(data){
                $('#hasil_buku').empty()
                console.log(data)
                for(i = 0; i < data.items.length ; i++){

                    data_buku_default += '<tr>';
                    data_buku_default += '<td>' +  data.items[i].volumeInfo.title + '</td>';
                    if( typeof (data.items[i].volumeInfo.authors) === 'undefined' ){
                        data_buku_default += "<td> No Author  </td>";   
                    }else{
                        data_buku_default += '<td>' +  data.items[i].volumeInfo.authors + '</td>';
                    }
                    if( (typeof(data.items[i].volumeInfo.imageLinks)) === 'undefined' ){
                        data_buku_default += "<td> No Thumbnail  </td>";   
                    }else{
                        data_buku_default += '<td>' + '<img src=' + data.items[i].volumeInfo.imageLinks.thumbnail + "> </td>";
                    }
                    data_buku_default += '</tr>'
        
                }
                console.log(data_buku_default)
                $("#hasil_buku").append(data_buku_default)

            },
            error: function(){
                alert("Something was wrong , Please try again")
            }
        });

        event.preventDefault();
    })
});